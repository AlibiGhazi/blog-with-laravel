<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;
use Schema;
use DB;

class install extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'blog:install';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'install the blog system';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        if(Schema::hasTable('migrations')){
            $this->info("The blog is already installed");
            $reset = $this->choice('reinstall blog ', ['Yes', 'No']);
            $this->info($reset);

            if($reset === "Yes"){
                $this->info("start reset");
                $tables = DB::select('SHOW TABLES');
                $bar = $this->output->createProgressBar(count($tables));

                foreach($tables as $table) {
                    Schema::drop($table->Tables_in_blog);
                    $bar->advance();
                    $this->info(" removing: ". $table->Tables_in_blog);
                }
                Artisan::call("migrate");
                Artisan::call("db:seed");
                $new_settings = new \App\Setting();
                $new_settings->blog_title = $this->ask('Blog title');
                $new_settings->blog_header_link = "uploads/default.png";
                $new_settings->blog_logo = "uploads/default_logo.png";
                $new_settings->blog_description = $this->ask('Blog description');
                $new_settings->save();

            }
        }else{
            Artisan::call("migrate");
            Artisan::call("db:seed");
            $new_settings = new \App\Setting();
            $new_settings->blog_title = $this->ask('Blog title');
            $new_settings->blog_header_link = "uploads/default.png";
            $new_settings->blog_logo = "uploads/default_logo.png";
            $new_settings->blog_description = $this->ask('Blog description');
            $new_settings->save();
        }
    /*    $setting = \App\Setting::find(1);
        if($setting){
            $this->info("The blog is already installed");
        }else{


        }*/
    }
}
