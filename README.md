# laravel-blog v0.5.19


Simple blog system with admin section



#Installtion:
After cloning from this repo , do the following steps : 

1. composer install
2. php artisan blog:install
3. default user & pass : admin & admin123



#TODO:
- [X] default theme
- [ ] Profile section
- [ ] Fix validation issues
- [ ] Support for multi-theme

